
export function getUser(userId, onSuccess) {
    try {
        fetch(`/api/user?id=${userId}`)
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                onSuccess(data);
            });
    } catch (error) {
        console.error("Failed to get user:", error);
    }
}

export function getNotes(userId, onSuccess) {
    try {
        fetch(`/api/notes?userid=${userId}`)
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                onSuccess(data);
            });
    } catch (error) {
        console.error("Failed to get notes:", error);
    }
}