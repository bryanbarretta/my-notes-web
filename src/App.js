import './App.css';
import React, { useState, useEffect } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {getNotes, getUser} from "./api/ApiRequests";

function App() {

  const USER_ID = 1;

  const [user, setUser] = useState(null);
  const [notes, setNotes] = useState(null);

  useEffect(() => {
    getUser(USER_ID, (data) => {
      setUser(data);
      console.log("User loaded");
    });
    getNotes(USER_ID, (data) => {
      setNotes(data);
      console.log("Notes loaded");
    });
  }, []);

  function renderNotes() {
    return <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableBody>
          {notes.map((note) => (
              <TableRow key={"note-" + note.id} >
                <TableCell width="150px" align="left">{new Date(note.timestamp).toLocaleString()}</TableCell>
                <TableCell width="10px"> | </TableCell>
                <TableCell align="left">{note.message}</TableCell>
              </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  }

  return (
    <div className="App">
      <br/>
      <b>{user ? "Hello " + user.name : "Loading user..."}</b>
      <br/>
      <br/>
      {notes ? renderNotes() : "Loading notes..."}
    </div>
  );
}

export default App;
